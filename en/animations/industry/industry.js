
function Industry(resources)
{
	Industry.resources = resources;
	
}
Industry.prototype = {
	init: function()
	{

		this.game = new Phaser.Game(800, 500, Phaser.CANVAS, 'industry', { preload: this.preload, create: this.create, update: this.update, render: 
		this.render,parent:this });
	},

	preload: function()
	{
		//659 139
		
		this.game.scale.maxWidth = 800;
		this.game.scale.maxHeight = 500;
		this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
		this.game.load.image('phone', Industry.resources.phone);
		//this.game.load.image('placement', Industry.resources.placement);

		
		for(var i = 0;i<3;i++)
		{
			cnt = i+1;
			this.game.load.image('icon_'+cnt, Industry.resources['icon_'+cnt]);
			this.game.load.image('line_'+cnt, Industry.resources['line_'+cnt]);
		}
		
		this.game.created = false;
		this.game.stage.backgroundColor = 'ffffff';

	},

	create: function(evt)
	{
		//to remove touch
		/*
		Phaser.Canvas.setTouchAction(this.game.canvas, "auto");
		this.game.input.touch.preventDefault = false;
		*/
		this.iconCoordinatesAr = [{x:143,y:175},{x:654,y:139},{x:635,y:245}];
		this.lineCoordinatesAr = [{x:235,y:175},{x:519,y:152},{x:520,y:218}];
		this.textCoordinatesAr = [{x:143,y:225},{x:654,y:189},{x:635,y:303}];
		//this.parent.placement = this.game.add.sprite(this.game.world.centerX,this.game.world.centerY,'placement');
		//this.parent.placement.anchor.set(0.5);
		if(this.game.created === false)
		{
			
			this.parent.phone = this.game.add.sprite(this.game.world.centerX,this.game.world.centerY,'phone');
			
			this.parent.phone.anchor.set(0.5);
			this.parent.iconAr = [];
			this.parent.textAr = [];
			this.parent.lineAr = [];
			this.parent.iconAnimAr = [];
			this.parent.textAnimAr = [];
			this.parent.lineAnimAr = [];
			var cnt = 0;
			this.parent.style = Industry.resources.textStyle_1;
			this.game.load.image('icon_'+cnt, Industry.resources[cnt+"_icon"]);
			for(var i = 0;i<3;i++)
			{
				cnt = i+1;
				var tempText = this.game.add.text(this.textCoordinatesAr[i].x,this.textCoordinatesAr[i].y, Industry.resources["icon_text_"+cnt],this.parent.style);
				tempText.id = cnt;
				tempText.inputEnabled = true;
				tempText.events.onInputDown.add(this.parent.actionOnClick, this);
				tempText.input.useHandCursor = true;
				tempText.input._setHandCursor = true;
				this.parent.textAr.push(tempText);
				this.parent.textAr[i].alpha = 0;
				this.parent.textAr[i].anchor.set(0.5,0.5);

				var temp_button = this.game.add.button(this.iconCoordinatesAr[i].x,this.iconCoordinatesAr[i].y,'icon_'+cnt,this.parent.actionOnClick,this);
				temp_button.smoothed = false;
				temp_button.id = cnt;
				this.parent.iconAr.push(temp_button);
				
				temp_button.useHandCursor = true;
				this.parent.iconAr[i].alpha = 0;
				this.parent.iconAr[i].anchor.set(0.5,0.5);
				
				
				//this.parent.textAr[i].anchor.set(0.5,0.5);
				this.parent.lineAr.push(this.game.add.sprite(this.lineCoordinatesAr[i].x,this.lineCoordinatesAr[i].y,'line_'+cnt));
				this.parent.lineAr[i].anchor.set(0.5,0.5);
				this.parent.lineAr[i].alpha = 0;
				
				

				this.parent.iconAnimAr.push(this.game.add.tween(this.parent.iconAr[i]).to({alpha:1,x:this.iconCoordinatesAr[i].x},500,Phaser.Easing.Quadratic.Out));
				this.parent.textAnimAr.push(this.game.add.tween(this.parent.textAr[i]).to({alpha:1},1000,Phaser.Easing.Quadratic.Out));
				this.parent.lineAnimAr.push(this.game.add.tween(this.parent.lineAr[i]).to({alpha:1},500,Phaser.Easing.Quadratic.Out));
				


			}

			this.game.created  = true;
			this.parent.iconAr[0].x-=100;
			this.parent.iconAr[1].x+=100;
			this.parent.iconAr[2].x+=120;
			this.parent.buildAnimation();
			
		}
	},

	actionOnClick: function(evt){
		
		this.parent.trigger("domPopup",{title:Industry.resources['icon_text_'+evt.id],body:Industry.resources['popupMessage_'+evt.id]});
	},
    
	buildAnimation: function()
	{
		
		this.iconAnimAr[0].chain(this.iconAnimAr[1],this.iconAnimAr[2],this.lineAnimAr[0],this.lineAnimAr[1],this.lineAnimAr[2],this.textAnimAr[0],this.textAnimAr[1],this.textAnimAr[2]);
		
		this.iconAnimAr[0].start();
		
	},
	inview: function()
	{
		
		
	},

	animate: function()
	{
		//console.log("animate")
	},

	update: function()
	{

	},
	render: function()
	{
		//this.game.debug.inputInfo(32, 32);
	}

}



