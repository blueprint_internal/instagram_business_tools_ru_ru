
function Reach(resources)
{
	Reach.resources = resources;
	
}
Reach.prototype = {
	init: function()
	{

		this.game = new Phaser.Game(800, 500, Phaser.CANVAS, 'reach', { preload: this.preload, create: this.create, update: this.update, render: 
		this.render,parent:this });
	},

	preload: function()
	{
		//659 139
		
		this.game.scale.maxWidth = 800;
		this.game.scale.maxHeight = 500;
		this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
		this.game.load.image('people', Reach.resources.people);
		this.game.load.image('placement', Reach.resources.placement);

		
		for(var i = 0;i<3;i++)
		{
			cnt = i+1;
			this.game.load.image('icon_'+cnt, Reach.resources['icon_'+cnt]);
			this.game.load.image('line_'+cnt, Reach.resources['line_'+cnt]);
		}
		
		this.game.created = false;
		this.game.stage.backgroundColor = 'ffffff';

	},

	create: function(evt)
	{
		//to remove touch
		/*
		Phaser.Canvas.setTouchAction(this.game.canvas, "auto");
		this.game.input.touch.preventDefault = false;
		*/
		this.iconCoordinatesAr = [{x:143,y:230},{x:this.game.world.centerX+23,y:70},{x:671,y:207}];
		this.lineCoordinatesAr = [{x:285,y:315},{x:this.game.world.centerX+5,y:250},{x:550,y:315}];
		
		//this.parent.placement = this.game.add.sprite(this.game.world.centerX,this.game.world.centerY,'placement');
		//this.parent.placement.anchor.set(0.5);
		if(this.game.created === false)
		{
			
			this.parent.people = this.game.add.sprite(this.game.world.centerX,this.game.world.centerY+180,'people');
			
			this.parent.people.anchor.set(0.5);
			this.parent.iconAr = [];
			this.parent.textAr = [];
			this.parent.lineAr = [];
			this.parent.iconAnimAr = [];
			this.parent.textAnimAr = [];
			this.parent.lineAnimAr = [];
			var cnt = 0;
			this.parent.style = Reach.resources.textStyle_1;
			this.game.load.image('icon_'+cnt, Reach.resources[cnt+"_icon"]);
			for(var i = 0;i<3;i++)
			{
				cnt = i+1;
				

				var temp_button = this.game.add.button(this.iconCoordinatesAr[i].x,this.iconCoordinatesAr[i].y,'icon_'+cnt,this.parent.actionOnClick,this);
				temp_button.smoothed = false;
				temp_button.id = cnt;
				temp_button.input.enabled = false;
				this.parent.iconAr.push(temp_button);
				this.parent.iconAr[i].alpha = 0;
				this.parent.iconAr[i].anchor.set(0.5,0.5);
				
				
				//this.parent.textAr[i].anchor.set(0.5,0.5);
				this.parent.lineAr.push(this.game.add.sprite(this.lineCoordinatesAr[i].x,this.lineCoordinatesAr[i].y,'line_'+cnt));
				this.parent.lineAr[i].anchor.set(0.5,0.5);
				this.parent.lineAr[i].alpha = 0;
				
				this.parent.people.alpha = 0;
				
				this.parent.iconAnimAr.push(this.game.add.tween(this.parent.iconAr[i]).to({alpha:1,y:this.iconCoordinatesAr[i].y},500,Phaser.Easing.Quadratic.Out));
				//this.parent.textAnimAr.push(this.game.add.tween(this.parent.textAr[i]).to({alpha:1},1000,Phaser.Easing.Quadratic.Out));
				this.parent.lineAnimAr.push(this.game.add.tween(this.parent.lineAr[i]).to({alpha:1,y:this.lineCoordinatesAr[i].y},500,Phaser.Easing.Quadratic.Out));
				this.parent.peopleAnim = this.game.add.tween(this.parent.people).to({alpha:1},1000,Phaser.Easing.Quadratic.Out)


			}

			this.game.created  = true;
			
			this.parent.iconAr[0].y-=200;
			this.parent.iconAr[1].y-=200;
			this.parent.iconAr[2].y-=200;
			//
			this.parent.lineAr[0].y+=20;
			this.parent.lineAr[1].y+=20;
			this.parent.lineAr[2].y+=20;

			this.parent.buildAnimation();
			
			
		}
	},

	actionOnClick: function(evt){
		
		this.parent.trigger("domPopup",{title:Reach.resources['icon_text_'+evt.id],body:Reach.resources['popupMessage_'+evt.id]});
	},
    
	buildAnimation: function()
	{
		
		this.peopleAnim.chain(this.iconAnimAr[0],this.lineAnimAr[0],this.iconAnimAr[1],this.lineAnimAr[1],this.iconAnimAr[2],this.lineAnimAr[2]);
		
		this.peopleAnim.start();
		
	},
	inview: function()
	{
		
		
	},

	animate: function()
	{
		//console.log("animate")
	},

	update: function()
	{

	},
	render: function()
	{
		//this.game.debug.inputInfo(32, 32);
	}

}



